package br.com.kaeff.back.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-09-22T00:18:41.383-0300")
@StaticMetamodel(Lista.class)
public class Lista_ {
	public static volatile SingularAttribute<Lista, Integer> id;
	public static volatile SingularAttribute<Lista, String> nome;
	public static volatile ListAttribute<Lista, ProdutoLista> produtos;
}
