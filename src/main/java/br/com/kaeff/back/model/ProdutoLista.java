package br.com.kaeff.back.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@NamedQueries({
	@NamedQuery(name="ProdutoLista.buscaTodos",
		query="select p from ProdutoLista p"),
	@NamedQuery(name="ProdutoLista.buscaProdutosPorSituacao",
		query="select p from ProdutoLista p where p.lista = :lista and p.situacao = :situacao"),
	@NamedQuery(name="ProdutoLista.buscaProdutosPorLista",
		query="select p from ProdutoLista p where p.lista = :lista"),
	@NamedQuery(name="ProdutoLista.buscaListasPorProduto",
		query="select p from ProdutoLista p where p.produto = :produto"),
	@NamedQuery(name="ProdutoLista.deletaPorId",
		query="delete from ProdutoLista p where p.id = :id")
})
@Entity
@Table(name = "listas_produtos", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"i_listas", "i_produtos"})})
public class ProdutoLista implements Serializable {
	private static final long serialVersionUID = -6316877321024373433L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "i_listas_produtos")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="i_listas")
	@NotNull
	@JsonManagedReference
	private Lista lista;

	@ManyToOne
	@JoinColumn(name="i_produtos")
	@NotNull
	@JsonManagedReference
	private Produto produto;
	
	@NotNull
	private BigDecimal quantidade;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private SituacaoProdutoLista situacao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Lista getLista() {
		return lista;
	}

	public void setLista(Lista lista) {
		this.lista = lista;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public SituacaoProdutoLista getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoProdutoLista situacao) {
		this.situacao = situacao;
	}
	
}
