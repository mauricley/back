package br.com.kaeff.back.model;

public enum SituacaoProdutoLista {
	PENDENTE, COMPRADO;
}
