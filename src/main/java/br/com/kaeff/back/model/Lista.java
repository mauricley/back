package br.com.kaeff.back.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@NamedQueries({
	@NamedQuery(name="Lista.buscaTodos", query="select l from Lista l")
})
@Entity
@Table(name = "listas")
public class Lista implements Serializable {
	private static final long serialVersionUID = 1177323608255583225L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "i_listas")
	private Integer id;
	
	@NotNull
	private String nome;
	
	@OneToMany(mappedBy = "lista", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JsonBackReference
	private List<ProdutoLista> produtos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<ProdutoLista> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoLista> produtos) {
		this.produtos = produtos;
	}
	
	public void adicionaProduto(ProdutoLista produto) {
		if (!this.produtos.contains(produto))
			this.produtos.add(produto);
	}

}
