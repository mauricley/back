package br.com.kaeff.back.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

@NamedQueries({
	@NamedQuery(name="Produto.buscaTodos", query="SELECT p FROM Produto p"),
	@NamedQuery(name="Produto.buscaProdutosForaLista",
		query="SELECT p FROM Produto p WHERE not exists(SELECT l FROM ProdutoLista l WHERE l.produto = p and l.lista = :lista)")
})
@Entity
@Table(name = "produtos")
public class Produto implements Serializable {
	private static final long serialVersionUID = -7538490036205424558L;

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "i_produtos")
	private Integer id;
	
	@NotNull
	private String nome;
	
	@NotNull
	private BigDecimal preco;
	
	@OneToMany(mappedBy = "produto", fetch=FetchType.EAGER)
	@JsonBackReference
	private List<ProdutoLista> listas;
	
	public Produto() {
		super();
		this.preco = new BigDecimal(0.00);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public List<ProdutoLista> getListas() {
		return listas;
	}

	public void setListas(List<ProdutoLista> listas) {
		this.listas = listas;
	}
	

}
