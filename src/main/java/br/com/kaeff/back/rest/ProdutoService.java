/**
 * 
 */
package br.com.kaeff.back.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.kaeff.back.dao.ProdutoDAO;
import br.com.kaeff.back.model.Produto;

@RequestScoped
@Path("produto")
public class ProdutoService {
	
	@Inject
	private ProdutoDAO dao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(final Produto produto) {
		Produto l;
		try {
			l = dao.salva(produto);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		return Response.ok(l).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") final int id) {
		Produto produto = dao.buscaPorId(id);
		if (produto == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(produto).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		List<Produto> produtos = dao.buscaTodos();
		GenericEntity<List<Produto>> geProdutos = new GenericEntity<List<Produto>>(produtos){};
		return Response.ok(geProdutos).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, final Produto produto) {
		if (id != produto.getId())
			return Response.status(Status.BAD_REQUEST).build();
			
		if (dao.buscaPorId(id) == null)
			return Response.status(Status.NOT_FOUND).build();
		
		return Response.ok(dao.salva(produto)).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteById(@PathParam("id") final int id) {
		Produto produto = dao.buscaPorId(id);
		
		if (produto == null)
			return Response.status(Status.NOT_FOUND).build();
		
		try {
			dao.exclui(produto);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.ok().build();
	}
}
