/**
 * 
 */
package br.com.kaeff.back.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("rest")
public class AppService extends Application {

}
