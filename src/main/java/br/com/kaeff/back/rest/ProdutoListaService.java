/**
 * 
 */
package br.com.kaeff.back.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.kaeff.back.dao.ListaDAO;
import br.com.kaeff.back.dao.ProdutoDAO;
import br.com.kaeff.back.dao.ProdutoListaDAO;
import br.com.kaeff.back.model.Lista;
import br.com.kaeff.back.model.Produto;
import br.com.kaeff.back.model.ProdutoLista;

@RequestScoped
@Path("produtoLista")
public class ProdutoListaService {
	
	@Inject
	private ProdutoListaDAO dao;
	@Inject
	private ProdutoDAO produtoDAO;
	@Inject
	private ListaDAO listaDAO;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(final ProdutoLista produtoLista) {
		ProdutoLista l;
		try {
			l = dao.salva(produtoLista);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		return Response.ok(l).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") final int id) {
		ProdutoLista produtoLista = dao.buscaPorId(id);
		if (produtoLista == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(produtoLista).build();
	}

	@GET
	@Path("/lista/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAllByListaId(@PathParam("id") final int id) {
		Lista lista = listaDAO.buscaPorId(id);
		
		if (lista == null)
			return Response.status(Status.NOT_FOUND).build();
		
		List<ProdutoLista> produtoListas = dao.buscaProdutosPorLista(lista);
		GenericEntity<List<ProdutoLista>> geProdutoListas = 
				new GenericEntity<List<ProdutoLista>>(produtoListas){};
		
		return Response.ok(geProdutoListas).build();
	}
	
	@GET
	@Path("/produto/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAllByProdutoId(@PathParam("id") final int id) {
		Produto produto = produtoDAO.buscaPorId(id);
		
		if (produto == null)
			return Response.status(Status.NOT_FOUND).build();
		
		List<ProdutoLista> produtoListas = dao.buscaListasPorProduto(produto);
		GenericEntity<List<ProdutoLista>> geProdutoListas = 
				new GenericEntity<List<ProdutoLista>>(produtoListas){};
		
		return Response.ok(geProdutoListas).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		List<ProdutoLista> produtoListas = dao.buscaTodos();
		GenericEntity<List<ProdutoLista>> geProdutoListas = 
				new GenericEntity<List<ProdutoLista>>(produtoListas){};
		
		return Response.ok(geProdutoListas).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, final ProdutoLista produtoLista) {
		if (id != produtoLista.getId())
			return Response.status(Status.BAD_REQUEST).build();
			
		if (dao.buscaPorId(id) == null)
			return Response.status(Status.NOT_FOUND).build();
		
		return Response.ok(dao.salva(produtoLista)).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteById(@PathParam("id") final int id) {
		try {
			dao.exclui(id);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.ok().build();
	}
}
