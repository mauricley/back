/**
 * 
 */
package br.com.kaeff.back.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.kaeff.back.dao.ListaDAO;
import br.com.kaeff.back.model.Lista;

@RequestScoped
@Path("lista")
public class ListaService {
	
	@Inject
	private ListaDAO dao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(final Lista lista) {
		Lista l;
		try {
			l = dao.salva(lista);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		return Response.ok(l).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") final int id) {
		Lista lista = dao.buscaPorId(id);
		if (lista == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(lista).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		List<Lista> listas = dao.buscaTodos();
		GenericEntity<List<Lista>> geListas = new GenericEntity<List<Lista>>(listas){};
		return Response.ok(geListas).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, final Lista lista) {
		if (id != lista.getId())
			return Response.status(Status.BAD_REQUEST).build();
			
		if (dao.buscaPorId(id) == null)
			return Response.status(Status.NOT_FOUND).build();
		
		return Response.ok(dao.salva(lista)).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteById(@PathParam("id") final int id) {
		Lista lista = dao.buscaPorId(id);
		
		if (lista == null)
			return Response.status(Status.NOT_FOUND).build();
		
		try {
			dao.exclui(lista);
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.ok().build();
	}
}
