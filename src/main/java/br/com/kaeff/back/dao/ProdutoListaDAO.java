package br.com.kaeff.back.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.kaeff.back.model.Lista;
import br.com.kaeff.back.model.Produto;
import br.com.kaeff.back.model.ProdutoLista;
import br.com.kaeff.back.model.SituacaoProdutoLista;


@Stateless
public class ProdutoListaDAO implements Serializable {
	private static final long serialVersionUID = 6023364780741531065L;
	
	@PersistenceContext(unitName = "back")
    private EntityManager em;
	
	public ProdutoLista buscaPorId(Integer id) {
		return em.find(ProdutoLista.class, id);
	}
	
	public List<ProdutoLista> buscaTodos() {
		return em.createNamedQuery("ProdutoLista.buscaTodos", ProdutoLista.class).getResultList();
	}
	
	public List<ProdutoLista> buscaProdutosPorSituacao(Lista lista, SituacaoProdutoLista situacao) {
		TypedQuery<ProdutoLista> qry = em.createNamedQuery("ProdutoLista.buscaProdutosPorSituacao", ProdutoLista.class);
		return qry.setParameter("lista", lista).setParameter("situacao", situacao).getResultList();
	}
	
	public List<ProdutoLista> buscaListasPorProduto(Produto produto) {
		TypedQuery<ProdutoLista> qry = em.createNamedQuery("ProdutoLista.buscaListasPorProduto", ProdutoLista.class);
		return qry.setParameter("produto", produto).getResultList();
	}
	
	public List<ProdutoLista> buscaProdutosPorLista(Lista lista) {
		TypedQuery<ProdutoLista> qry = em.createNamedQuery("ProdutoLista.buscaProdutosPorLista", ProdutoLista.class);
		return qry.setParameter("lista", lista).getResultList();
	}
	
	
	public ProdutoLista salva(ProdutoLista produtoLista) {
		if(produtoLista.getId() == null) {
			em.persist(produtoLista);
			return produtoLista;
		} else
			return em.merge(produtoLista);
	}

	public void exclui(Integer id) {
		em.createNamedQuery("ProdutoLista.deletaPorId").setParameter("id", id).executeUpdate();
	}
}
