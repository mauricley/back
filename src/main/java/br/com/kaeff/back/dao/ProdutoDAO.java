package br.com.kaeff.back.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.kaeff.back.model.Lista;
import br.com.kaeff.back.model.Produto;

@Stateless
public class ProdutoDAO implements Serializable {
	private static final long serialVersionUID = 702336988108826027L;
	
	@PersistenceContext(unitName = "back")
    private EntityManager em;
	
	public Produto buscaPorId(Integer id) {
		return em.find(Produto.class, id);
	}
		
	public List<Produto> buscaTodos() {
		return em.createNamedQuery("Produto.buscaTodos", Produto.class).getResultList();
	}
	
	public List<Produto> buscaProdutosForaLista(Lista lista) {
		TypedQuery<Produto> qry = em.createNamedQuery("Produto.buscaProdutosForaLista", Produto.class);
		return qry.setParameter("lista", lista).getResultList();
	}
	
	public Produto salva(Produto produto) {
		if (produto.getId() == null) { 
			em.persist(produto);
			return produto;
		} else
			return em.merge(produto);
	}

	public void exclui(Produto produto) {
		em.remove(em.contains(produto) ? produto : em.merge(produto));
	}
}
