package br.com.kaeff.back.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.kaeff.back.model.Lista;


@Stateless
public class ListaDAO implements Serializable {
	private static final long serialVersionUID = -5925575486344964449L;
	
	@PersistenceContext(unitName = "back")
    private EntityManager em;
	
	public Lista buscaPorId(Integer id) {
		return em.find(Lista.class, id);
	}
		
	public List<Lista> buscaTodos() {
		return em.createNamedQuery("Lista.buscaTodos", Lista.class).getResultList();
	}

	public Lista salva(Lista lista) {
		if (lista.getId() == null) { 
			em.persist(lista);
			return lista;
		} else
			return em.merge(lista);
	}

	public void exclui(Lista lista) {
		em.remove(em.contains(lista) ? lista : em.merge(lista));
	}
}
