package br.com.kaeff.back.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import br.com.kaeff.back.dao.ProdutoDAO;
import br.com.kaeff.back.model.Produto;

@ManagedBean
public class ProdutoBean {
	private Produto produto = new Produto();
	@Inject
	private ProdutoDAO dao;
	
	public ProdutoBean() {
		super();
		novoProduto();
	}

	
	public Produto getProduto() {
		return produto;
	}
	
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
		
	public List<Produto> getProdutos() {
		return this.dao.buscaTodos();
	}
	
	public void novoProduto() {
		this.produto = new Produto();
	}
	
	public void gravar() {
		dao.salva(produto);
		novoProduto();
	}
	
	public void editar(Produto produto) {
		this.produto = produto; 
	}
	
	public void excluir(Produto produto) {
		dao.exclui(produto);
		novoProduto();
	}

}