package br.com.kaeff.back.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import br.com.kaeff.back.dao.ListaDAO;
import br.com.kaeff.back.model.Lista;

@ManagedBean
public class ListaBean {
	private Lista lista;
	@Inject
	private ListaDAO dao;
	
	public ListaBean() {
		super();
		novaLista();
	}
		
	public Lista getLista() {
		return lista;
	}

	public void setLista(Lista lista) {
		this.lista = lista;
	}

	public List<Lista> getListas() {
		return dao.buscaTodos();
	}
	
	public void novaLista() {
		this.lista = new Lista();
	}

	public void gravar() {
		dao.salva(lista);
		novaLista();
	}
	
	public void editar(Lista lista) {
		this.lista = lista;
	}
	
	public void excluir(Lista lista) {
		dao.exclui(lista);
		novaLista();
	}
}
