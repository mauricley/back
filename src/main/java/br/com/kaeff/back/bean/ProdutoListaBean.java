package br.com.kaeff.back.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import br.com.kaeff.back.dao.ListaDAO;
import br.com.kaeff.back.dao.ProdutoDAO;
import br.com.kaeff.back.dao.ProdutoListaDAO;
import br.com.kaeff.back.model.Lista;
import br.com.kaeff.back.model.Produto;
import br.com.kaeff.back.model.ProdutoLista;
import br.com.kaeff.back.model.SituacaoProdutoLista;

@ManagedBean
@ViewScoped
public class ProdutoListaBean {
	private Lista lista;
	@Inject
	private ProdutoListaDAO dao;
	@Inject
	private ProdutoDAO produtoDAO;
	@Inject
	private ListaDAO listaDAO;
	
	private BigDecimal totalPendente;
	private BigDecimal totalComprado;
	
	private BigDecimal somaTotal(List<ProdutoLista> produtoListas) {
		BigDecimal total = new BigDecimal(0);
		
		for (ProdutoLista item : produtoListas) {
			total = total.add(item.getQuantidade().multiply(item.getProduto().getPreco()));
		}
		
		return total;
	}
	
	private List<ProdutoLista> buscaProdutosPorSituacao(SituacaoProdutoLista situacao) {
		return dao.buscaProdutosPorSituacao(lista, situacao);
	}
	
	@PostConstruct
	public void init() {
		Integer id = Integer.parseInt(FacesContext.getCurrentInstance()
									  .getExternalContext().getRequestParameterMap()
									  .get("listaId"));
		lista = listaDAO.buscaPorId(id);
	}
	
	public Lista getLista() {
		return this.lista;
	}
	
	public BigDecimal getTotalPendente() {
		return totalPendente;
	}
	
	public BigDecimal getTotalComprado() {
		return totalComprado;
	}
	
	public SituacaoProdutoLista getSituacaoPendente() {
		return SituacaoProdutoLista.PENDENTE;
	}
	
	public SituacaoProdutoLista getSituacaoComprado() {
		return SituacaoProdutoLista.COMPRADO;
	}
	
	public List<ProdutoLista> getProdutosPendentes() {
		List<ProdutoLista> produtosPendentes = this.buscaProdutosPorSituacao(SituacaoProdutoLista.PENDENTE);
		this.totalPendente = somaTotal(produtosPendentes);
		return produtosPendentes;
	}
	
	public List<ProdutoLista> getProdutosComprados() {
		List<ProdutoLista> produtosComprados = this.buscaProdutosPorSituacao(SituacaoProdutoLista.COMPRADO);
		this.totalComprado = somaTotal(produtosComprados);
		return produtosComprados;
	}
	
	/**
	 * Retorna os produtos ainda não selecionados para a lista
	 */
	public List<ProdutoLista> getProdutosForaLista() {
		ProdutoLista item;
		List<ProdutoLista> itens = new ArrayList<ProdutoLista>();
		List<Produto> produtos = produtoDAO.buscaProdutosForaLista(lista);
		for (Produto produto : produtos) {
			item = new ProdutoLista();
			item.setLista(lista);
			item.setProduto(produto);
			item.setQuantidade(new BigDecimal("1.00"));
			item.setSituacao(SituacaoProdutoLista.PENDENTE);
			itens.add(item);
		}
		return itens;
	}
	
	public void gravar(ProdutoLista produtoLista) {
		dao.salva(produtoLista);
	}
	
	public void excluir(Integer id) {
		dao.exclui(id);
	}
	
	public void alterarSituacao(ProdutoLista produtoLista, SituacaoProdutoLista situacao) {
		produtoLista.setSituacao(situacao);
		gravar(produtoLista);
	}
}
