package br.com.kaeff.back.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-09-22T00:18:41.530-0300")
@StaticMetamodel(ProdutoLista.class)
public class ProdutoLista_ {
	public static volatile SingularAttribute<ProdutoLista, Integer> id;
	public static volatile SingularAttribute<ProdutoLista, Lista> lista;
	public static volatile SingularAttribute<ProdutoLista, Produto> produto;
	public static volatile SingularAttribute<ProdutoLista, BigDecimal> quantidade;
	public static volatile SingularAttribute<ProdutoLista, SituacaoProdutoLista> situacao;
}
